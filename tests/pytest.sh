#!/bin/sh

set -e -x

export OS_NAME=posix;
cd /coala-bears;

# Currently not installed with playbook
rm bears/Constants.py;
rm bears/c_languages/CSharpLintBear.py tests/c_languages/CSharpLintBearTest.py;
rm bears/c_languages/ClangBear.py tests/c_languages/ClangBearTest.py;
rm bears/c_languages/ClangComplexityBear.py tests/c_languages/ClangComplexityBearTest.py;
rm -r bears/c_languages/codeclone_detection/ tests/c_languages/codeclone_detection/;
rm bears/configfiles/PuppetLintBear.py tests/configfiles/PuppetLintBearTest.py;
rm bears/configfiles/TOMLBear.py tests/configfiles/TOMLBearTest.py;
rm bears/csv/CSVLintBear.py tests/csv/CSVLintBearTest.py;
rm bears/dart/DartLintBear.py tests/dart/DartLintBearTest.py;
rm bears/documentation/DocGrammarBear.py tests/documentation/DocGrammarBearTest.py;
rm bears/elm/ElmLintBear.py tests/elm/ElmLintBearTest.py;
rm bears/general/CPDBear.py tests/general/CPDBearTest.py;
rm -r bears/go tests/go;
rm bears/haml/HAMLLintBear.py tests/haml/HAMLLintBearTest.py;
rm bears/java/InferBear.py tests/java/InferBearTest.py;
rm bears/java/JavaPMDBear.py tests/java/JavaPMDBearTest.py;
rm bears/julia/JuliaLintBear.py tests/julia/JuliaLintBearTest.py;
rm bears/lua/LuaLintBear.py tests/lua/LuaLintBearTest.py;
rm bears/natural_language/LanguageToolBear.py tests/natural_language/LanguageToolBearTest.py;
rm bears/perl/PerlCriticBear.py tests/perl/PerlCriticBearTest.py;
rm bears/python/requirements/PySafetyBear.py tests/python/requirements/PySafetyBearTest.py;
rm tests/python/requirements/PySafetyBearWithoutMockTest.py;
rm bears/r/RLintBear.py tests/r/RLintBearTest.py;
rm bears/ruby/RuboCopBear.py tests/ruby/RuboCopBearTest.py;
rm bears/ruby/RubyFastererBear.py tests/ruby/RubyFastererBearTest.py;
rm bears/ruby/RubySecurityBear.py tests/ruby/RubySecurityBearTest.py;
rm bears/ruby/RubySmellBear.py tests/ruby/RubySmellBearTest.py;
rm bears/scss/SCSSLintBear.py tests/scss/SCSSLintBearTest.py;
rm bears/sql/SQLintBear.py tests/sql/SQLintBearTest.py;
rm bears/swift/TailorBear.py tests/swift/TailorBearTest.py;
rm bears/typescript/TSLintBear.py tests/typescript/TSLintBearTest.py;
rm bears/verilog/VerilogLintBear.py tests/verilog/VerilogLintBearTest.py;
rm bears/vhdl/VHDLLintBear.py tests/vhdl/VHDLLintBearTest.py;
rm bears/yaml/TravisLintBear.py tests/yaml/TravisLintBearTest.py;
rm bears/haskell/GhcModBear.py  tests/haskell/GhcModBearTest.py
rm bears/shell/ShellCheckBear.py tests/shell/ShellCheckBearTest.py
rm bears/vimscript/VintBear.py tests/vimscript/VintBearTest.py
rm bears/python/PEP8NotebookBear.py tests/python/PEP8NotebookBearTest.py
rm bears/python/PEP8Bear.py tests/python/PEP8BearTest.py

python3 -m pytest --cov --cov-fail-under=100;
