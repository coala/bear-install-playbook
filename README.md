# bear-install-playbook

This repository contains ansible playbooks for installing bears and their
dependencies. Ansible can be simply installed using the pip package manager
as follows:

```shell
pip install -U ansible
```

Visit [here](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
for more instructions or information regarding installing ansible
using a different package manager.

To use the playbook, run:

```shell
ansible-playbook main.yml
```

Make sure you are inside the playbooks directory while running this. The playbook
will prompt you to list the names of the bears that you want to install. At this
point enter a comma separated list of bear names that you would like to install
or a single bear name without a comma.

Please read the logs from ansible for more information in case of
failure during installation.
